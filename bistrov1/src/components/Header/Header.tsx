import React, {
  useEffect,
  useRef,
  useState,
  MouseEvent,
} from 'react';
import styles from '@/components/Header/Header.module.css';
import Image from 'next/image';

export default function Header(props: {
  isOpen: boolean;
  toggleMenu: () => void;
  scrollPosition: number;
}) {
  const [headerId, setHeaderId] = useState(styles.topHeader);
  const [hamburgerColor, setHambergerColor] = useState(true);
  const [horaireOpen, setHoraireOpen] = useState(true);
  const [isBtnReservationClicked, setBtnReservationClicked] =
    useState(false);

  const btnReservation = () => {
    setBtnReservationClicked(!isBtnReservationClicked);
  };

  const scrollToSection = (
    sectionId: string,
    event: MouseEvent<HTMLAnchorElement>
  ) => {
    event.preventDefault(); // Empêche le comportement par défaut du lien
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const togglHoraire = () => {
    setHoraireOpen(!horaireOpen);
  };
  useEffect(() => {
    if (props.scrollPosition > 330) {
      setHeaderId(styles.topHeaderScrolled);
      setHambergerColor(true);
    } else {
      setHeaderId(styles.topHeader);
      setHambergerColor(false);
    }
  }, [props.scrollPosition]);
  return (
    <header id={styles.header}>
      <section className={styles.topHeader} id={headerId}>
        {/* Version Mobile */}
        {props.isOpen && (
          <div className={styles.socialNetwork}>
            <div className={styles.instagram}>
              <a href='https://www.instagram.com/bistrotduphare17/'>
                <Image
                  className={styles.logoNetwork}
                  src={`/image/instagram.svg`}
                  alt='Logo instagram qui redirige sur le site instagram du restaurant'
                />
              </a>
            </div>
          </div>
        )}
        <div id={styles.logo}>
          <Image
            src={`/image/logo.jpg`}
            width={30}
            height={30}
            alt='Logo du bistrot du phare'
          />
        </div>
        {/* Version plus grande */}
        {!props.isOpen && (
          <>
            <div className={styles.desktopMenu}>
              <a
                className={styles.link}
                onClick={(e) => scrollToSection('info-nous', e)}
              >
                <div className={styles.menuItem}>
                  À propos de nous
                </div>
              </a>
              <a
                className={styles.link}
                onClick={(e) => scrollToSection('carte', e)}
              >
                <div className={styles.menuItem}>La carte</div>
              </a>
              <a
                className={styles.link}
                onClick={(e) => scrollToSection('galerie', e)}
              >
                <div className={styles.menuItem}>Galerie</div>
              </a>
              <a
                className={styles.link}
                onClick={(e) => scrollToSection('recrutement', e)}
              >
                <div className={styles.menuItem}>Recrutement</div>
              </a>
              <a
                className={styles.link}
                onClick={(e) => scrollToSection('geocalisation', e)}
              >
                <div className={styles.menuItem}>
                  Où sommes-nous ?
                </div>
              </a>
            </div>
            <div className={styles.socialNetworkDesktop}>
              {/* ... Icônes des réseaux sociaux ... */}
              <div className={styles.instagram}>
                <a href='https://www.instagram.com/bistrotduphare17/'>
                  <Image
                    className={styles.logoNetwork}
                    src={`/image/instagram.svg`}
                    alt='Logo instagram qui redirige sur le site instagram du restaurant'
                  />
                </a>
              </div>
            </div>
          </>
        )}

        <div
          className={`${styles.hamburger} ${
            props.isOpen ? styles.open : ''
          }`}
          onClick={props.toggleMenu}
        >
          <span
            className={`${styles.bar}  ${
              hamburgerColor ? styles.blackcolor : ''
            }`}
          ></span>
          <span
            className={`${styles.bar}  ${
              hamburgerColor ? styles.blackcolor : ''
            }`}
          ></span>
          <span
            className={`${styles.bar}  ${
              hamburgerColor ? styles.blackcolor : ''
            }`}
          ></span>
        </div>
        {props.isOpen && (
          <div className={styles.dropdownMenu}>
            <a
              onClick={(e) => {
                scrollToSection('info-nous', e);
                props.toggleMenu();
              }}
              className={styles.linkTel}
            >
              <div className={styles.menuItem}>À PROPOS DE NOUS</div>
            </a>
            <div className={styles.menuDivider}></div>
            <a
              className={styles.linkTel}
              onClick={(e) => {
                scrollToSection('carte', e);
                props.toggleMenu();
              }}
            >
              <div className={styles.menuItem}>LA CARTE</div>
            </a>
            <div className={styles.menuDivider}></div>

            <a
              className={styles.linkTel}
              onClick={(e) => {
                scrollToSection('galerie', e);
                props.toggleMenu();
              }}
            >
              <div className={styles.menuItem}>GALERIE</div>
            </a>
            <div className={styles.menuDivider}></div>
            <a
              className={styles.linkTel}
              onClick={(e) => {
                scrollToSection('recrutement', e);
                props.toggleMenu();
              }}
            >
              <div className={styles.menuItem}>RECRUTEMENT</div>
            </a>
            <div className={styles.menuDivider}></div>
            <a
              className={styles.linkTel}
              onClick={(e) => {
                scrollToSection('geocalisation', e);
                props.toggleMenu();
              }}
            >
              <div className={styles.menuItem}>OÙ SOMMES-NOUS</div>
            </a>
            <div className={styles.menuDivider}></div>
            <button
              id={styles.btnReservation}
              onClick={btnReservation}
            >
              <div className={styles.menuItem}>RÉSERVER</div>
            </button>

            <a
              href='tel:0546234362'
              className={`${styles.containerNumber}  ${
                isBtnReservationClicked
                  ? styles.containerNumberOpen
                  : ''
              }`}
            >
              <div className={styles.arrowtop}></div>
              <Image
                className={styles.svgPhone}
                src={`/image/phone-solid.svg`}
                alt="SVG d'un téléphone"
              />
              <span id={styles.number}>+335 46 23 43 62</span>
            </a>
          </div>
        )}
      </section>
      <Image
        id={styles.imageBackground}
        layout='fill'
        objectFit='cover'
        src={`/image/img_header.svg`}
        alt='Image du bistro du phare'
      />
      <div id={styles.headerTextCenter}>
        <h2 id={styles.title}>Le Bistrot du phare</h2>
        <div id={styles.horaire}>
          <div id={styles.headerHoraire} onClick={togglHoraire}>
            <p>Horaires d&apos;ouvertures</p>
            <div>
              <Image
                className={`${styles.chevron} ${
                  horaireOpen ? styles.chevronOpen : ''
                }`}
                src={`/image/chevron-right-solid.svg`}
                alt="chevron pour ouvrir les horaires d'ouvertures"
              />
            </div>
          </div>
          <div
            className={`${styles.horaireContainer} ${
              horaireOpen ? styles.horaireContainerVisible : ''
            }`}
          >
            <p className={styles.horaireItem}>
              <span>Lundi</span>Fermé
            </p>
            <p className={styles.horaireItem}>
              <span>Mardi</span>12h-18h30
            </p>
            <p className={styles.horaireItem}>
              <span>Mercredi</span>12h-18h30
            </p>
            <p className={styles.horaireItem}>
              <span>Jeudi</span>2h-18h30
            </p>
            <p className={styles.horaireItem}>
              <span>Vendredi</span>2h-18h30
            </p>
            <p className={styles.horaireItem}>
              <span>Samedi</span>2h-18h30
            </p>
          </div>
        </div>
      </div>
    </header>
  );
}
