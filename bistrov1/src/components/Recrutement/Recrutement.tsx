// components/RecruitmentForm.tsx
import { useState, FormEvent, ChangeEvent } from 'react';
import styles from '@/components/Recrutement/Recrutement.module.css';

interface FormData {
  name: string;
  email: string;
  message: string;
}

const RecrutementForm = () => {
  const [formData, setFormData] = useState<FormData>({
    name: '',
    email: '',
    message: '',
  });

  const handleChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const response = await fetch('/api/sendEmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });

    if (response.ok) {
      alert('Candidature envoyée avec succès !');
    } else {
      alert("Erreur lors de l'envoi de la candidature.");
    }
  };

  return (
    <form id={styles.form} onSubmit={handleSubmit}>
      <input
        className={styles.inputForm}
        type='text'
        name='name'
        placeholder='Votre nom'
        value={formData.name}
        onChange={handleChange}
      />
      <input
        className={styles.inputForm}
        type='email'
        name='email'
        placeholder='Votre email'
        value={formData.email}
        onChange={handleChange}
      />
      <textarea
        className={styles.inputForm}
        id={styles.textMessageForm}
        name='message'
        placeholder='Votre message'
        value={formData.message}
        onChange={handleChange}
      ></textarea>
      <button className={styles.btnForm} type='submit'>
        Envoyer
      </button>
    </form>
  );
};

export default RecrutementForm;
