import styles from '@/components/Geocalisation/Geocalisation.module.css';

const Geocalisation = () => {
  return (
    <div className={styles.homeWrapper}>
      <iframe
        src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d89247.10681043142!2d-1.2043357813994453!3d45.65138228131322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48017974c523ee57%3A0xe7581894ee7db76a!2sLe%20bistrot%20du%20phare!5e0!3m2!1sfr!2sfr!4v1707249196034!5m2!1sfr!2sfr'
        width='100%'
        height='450'
        allowFullScreen={true}
        loading='lazy'
        referrerPolicy='no-referrer-when-downgrade'
      ></iframe>
    </div>
  );
};

export default Geocalisation;
