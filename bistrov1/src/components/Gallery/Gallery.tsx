import React, { useState } from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // Importe les styles par défaut

import styles from '@/components/Gallery/Gallery.module.css';
import { images } from '@/lib/images';
import Image from 'next/image';

const Gallery = () => {
  const [selectedImage, setSelectedImage] = useState(null);

  const openCarousel = (index: any) => {
    setSelectedImage(index);
  };

  const closeCarousel = () => {
    setSelectedImage(null);
  };

  return (
    <>
      <div id={styles.gallery}>
        {images.map((image, index) => {
          let styleImage = styles.image;

          if (index % 2 == 0) {
            styleImage = styles.imageReversed;
          }

          return (
            <div
              key={index}
              className={styles.containerImage}
              onClick={() => openCarousel(index)}
            >
              <Image
                className={styleImage}
                src={image.src}
                alt={image.alt}
              />
            </div>
          );
        })}
      </div>
      {selectedImage !== null && (
        <div className={styles.carouselOverlay}>
          <div className={styles.carouselContainer}>
            <button
              className={styles.closeButton}
              onClick={closeCarousel}
            >
              Fermer
            </button>
            <Carousel selectedItem={selectedImage} showThumbs={false}>
              {images.map((image, index) => (
                <div
                  key={index}
                  className={styles.containerImageInCarrousel}
                >
                  <Image
                    className={styles.imageInCarrousel}
                    src={image.src}
                    alt={image.alt}
                    objectFit='cover'
                  />
                </div>
              ))}
            </Carousel>
          </div>
        </div>
      )}
    </>
  );
};

export default Gallery;
