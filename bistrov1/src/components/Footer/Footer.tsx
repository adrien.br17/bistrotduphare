import React from 'react';
import Image from 'next/image';
import styles from '@/components/Footer/Footer.module.css';
export default function Footer() {
  return (
    <div className={styles.footerContainer}>
      <div id={styles.containerLogo}>
        <Image
          id={styles.logo}
          src={`/image/logo.jpg`}
          width={30}
          alt='Image de la vue sur le phare avec un couché de soleil'
        />
      </div>
      <div id={styles.footerTextCenter}>
        <p>COPYRIGHT &copy; 2024 bistrot-du-phare.com</p>
        <p>Mentions Légales</p>
      </div>
    </div>
  );
}
