export const images = [
  {
    src: `/galerie/galerie1.jpg`,
    alt: "Image d'un coucher de soleil devant le bistrot du phare",
  },
  {
    src: `/galerie/galerie1.jpg`,
    alt: "Image d'un poulpe cuisiné",
  },
  {
    src: `/galerie/galerie1.jpg`,
    alt: "Image d'une entrée, deux toast avec du beurre d'algue",
  },
  {
    src: `/galerie/galerie1.jpg`,
    alt: "Image d'un table à l'exterieur du bistrot du phare",
  },
  {
    src: `/galerie/galerie1.jpg`,
    alt: "Image d'un dessert au chocolat",
  },
  {
    src: `/galerie/galerie1.jpg`,
    alt: "Image d'une assiette d'huitre",
  },
];
