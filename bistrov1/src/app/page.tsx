'use client';
import Image from 'next/image';
import React, { useState, useEffect } from 'react';
import styles from './page.module.css';
import Gallery from '@/components/Gallery/Gallery';
import Header from '@/components/Header/Header';
import RecrutementForm from '@/components/Recrutement/Recrutement';
import Geocalisation from '@/components/Geocalisation/Geocalisation';
import Footer from '@/components/Footer/Footer';

export default function Home() {
  const [isOpen, setIsOpen] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(0);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  const handleScroll = () => {
    const position = window.scrollY;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    // Fonction de nettoyage lors du démontage du composant
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  return (
    <>
      <Header
        isOpen={isOpen}
        toggleMenu={toggleMenu}
        scrollPosition={scrollPosition}
      />
      <main className={styles.main}>
        <section id='info-nous' className={styles.aboutUs}>
          <div id={styles.imageAboutUs}>
            <Image
              src={`/public/poisson.jpg`}
              alt='Image qui montre le chef Jerome entrain de cuisiner'
            />
          </div>
          <div id={styles.textAboutUs}>
            <h2
              className={styles.titleSection}
              id={styles.titleAboutUs}
            >
              À propos de nous
            </h2>
            <p id={styles.presentationCuisineChef}>
              Ma passion pour la cuisine se traduit par des plats
              sincères et généreux, reflétant l&apos;âme de notre
              terroir. Chaque assiette est une invitation à partager
              ma joie de cuisiner avec les meilleurs produits de notre
              région.
            </p>
            <p id={styles.presentationChef}>
              Jerome 4ème sélection française Paul Bocuse, École Alain
              Ducasse, et école Gaston Lenotre
            </p>
          </div>
        </section>
        <section id='carte' className={styles.carte}>
          <div id={styles.imgCarte}>
            <Image
              src={`/public/poisson.jpg`}
              alt="Image d'un poisson du bistro du phare"
            />
          </div>
          <div id={styles.textCartes}>
            <h2
              className={styles.titleSection}
              id={styles.titleCarte}
            >
              La carte
            </h2>
            <div id={styles.descriptionAndBtn}>
              <p>
                Le Bistrot du Phare est l&apos;endroit parfait pour un
                repas délicieux et convivial à Saint-Palais-sur-Mer.
                Nous sommes à l&apos;écart du centre dans un cadre
                exceptionnel face à la mer avec une vue sur le phare
                de Cordouan et l&apos;entrée de l&apos;Estuaire. Nous
                sommes fiers de vous présenter notre nouvelle carte,
                qui vous offrira une expérience culinaire riche en
                saveurs et en textures. Notre menu a été soigneusement
                conçu pour répondre à tous les goûts et toutes les
                envies. Que vous soyez amateur de viande ou de
                poisson, végétarien ou amateur de plats classiques,
                nous avons quelque chose pour vous. Notre cuisine est
                100% fait maison. Notre chef utilise des ingrédients
                frais et de qualité, qui sont cuisinés avec passion
                pour vous offrir des plats savoureux et originaux. Nos
                viandes sont d&apos;origine française et de race, nos
                poissons selon arrivage ou du jour sont pêchés à la
                cotinière en face de chez nous. Le Bistrot du Phare
                est également l&apos;endroit idéal pour se détendre
                avec un verre. Nous avons une belle sélection de
                cocktails sans ou avec alcool. Nous avons hâte de vous
                accueillir au Bistrot du phare pour un repas
                inoubliable. Venez découvrir notre nouvelle carte et
                laissez-nous vous faire plaisir avec notre cuisine
                savoureuse et notre ambiance chaleureuse.
              </p>
              <a
                href='/carte/carte.pdf'
                download='carte.pdf'
                id={styles.btnMenu}
              >
                Voir le menu
              </a>
            </div>
          </div>
        </section>
        <section id='galerie' className={styles.containerGallery}>
          <h2
            className={styles.titleSection}
            id={styles.titleGallery}
          >
            Galerie
          </h2>
          <Gallery></Gallery>
        </section>
        <section
          id='recrutement'
          className={styles.containerRecrutement}
        >
          <h2
            className={styles.titleSection}
            id={styles.titleRecrutement}
          >
            Recrutement
          </h2>
          <RecrutementForm />
        </section>
        <section
          id='geocalisation'
          className={styles.containerGeocalisation}
        >
          <h2
            className={styles.titleSection}
            id={styles.titleGeocalisation}
          >
            Où sommes-nous ?
          </h2>
          <Geocalisation />
        </section>
      </main>
      <footer>
        <Footer></Footer>
      </footer>
    </>
  );
}
