// pages/api/sendEmail.ts
import type { NextApiRequest, NextApiResponse } from 'next';
import nodemailer from 'nodemailer';

type Data = {
    success: boolean;
    error?: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
    if (req.method === 'POST') {
        const { name, email, message } = req.body;

        // Remplacez par votre configuration réelle
        const transporter = nodemailer.createTransport({
            host: "smtp.orange.fr",
            port: 465,
            secure: false,
            auth: {
                user: 'votre-email@example.com',
                pass: 'votre-mot-de-passe',
            },
        });

        try {
            await transporter.sendMail({
                from: '"Formulaire de Recrutement" <votre-email@example.com>',
                to: "lebistrotduphare17420@orange.fr",
                subject: "Nouvelle candidature",
                html: `<p>Vous avez reçu une nouvelle candidature de ${name} (${email}):</p><p>${message}</p>`,
            });

            res.status(200).json({ success: true });
        } catch (error: any) {
            res.status(500).json({ success: false, error: error.message });
        }
    } else {
        res.setHeader('Allow', ['POST']);
        res.status(405).end(`Method ${req.method} Not Allowed`);
    }
}
